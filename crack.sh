#!/bin/sh

set -e

SRC='aspose-words-21.1-jdk17.jar'
DST='aspose-words-21.1-jdk17-cracked.jar'

echo '==> 官网下载（新下载的此破解已失效，请直接下载破解版）'
stat $SRC || wget https://repository.aspose.com/repo/com/aspose/aspose-words/21.1/$SRC

echo '==> 重命名'
rm -f $DST
cp $SRC $DST

echo '==> 用7z命令删除jar内的META-INF文件夹'
7z d $DST META-INF

echo '==> 执行破解,生成类'
javac -classpath .:javassist-3.24.1-GA.jar AsposeWords_21_1.java
java -classpath .:javassist-3.24.1-GA.jar AsposeWords_21_1

echo '==> 用jar命令更新jar内的类'
echo '替换前'
jar -tvf $DST|grep zzZE0
jar uvf $DST com/aspose/words/zzZE0.class
echo '替换后'
jar -tvf $DST|grep zzZE0
rm -rf com/

echo '==> 测试转换'
javac -classpath $DST Office2PdfTest.java
java -classpath .:$DST Office2PdfTest
